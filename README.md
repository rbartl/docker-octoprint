# docker-octoprint
OctoPrint Docker container, Thanks to : http://octoprint.org/

```
docker run -d -p 5001:5000 -p 8088:8088 --device=/dev/video0 --device=/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A403M08H-if00-port0:/dev/ttyUSB0 -v /octo2/data:/data rbartl/docker-octoprint
```

---
Changelog
---

* commit to upgrade image to current 2.7 python

---
CuraEngine Integration
--
CuraEngine is installed under:
```
/CuraEngine/CuraEngine
```
Please set it in the settings menu if you intend to use it.

---
Webcam Integration
---
1. First, bind the camera to the docker using --device=/dev/video0:/dev/videoX where videoX is your camera device on linux.
2. If camera supports only MJPEG formatting, please set YUV_CAMERA to false.
3. should add to config.yaml/Or UI:
```
webcam:
  stream: http://HOST:8088/?action=stream
  snapshot: http://HOST:8088/?action=snapshot
  ffmpeg: /usr/bin/avconv
```
NOTE: This is just basic support, need to add HAProxy to make it properly.
HAProxy Instructions: https://github.com/foosel/OctoPrint/wiki/Setup-on-a-Raspberry-Pi-running-Raspbian#make-everything-accessible-on-port-80
